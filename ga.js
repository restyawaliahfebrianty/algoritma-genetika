function calculateFitness(){
	var currentRecord = infinity;
	for(var i=0; i<population.lenght; i++){
		if(d < recordDistance){
			recordDistance = d;
			bestEver = population[i];
		}
		
		if(d < currentRecord){
			currentRecord = d;
			currentBest = population[i];
		}
		fitness[i] = 1/(pow(d, 8) + 1);
	}
}

function normalizeFitness(){
	var sum = 0;
	for(var i=0; i< fitness.lenght; i++){
		sum += fitness[i];
	}
	for(var i=0; i< fitness.lenght; i++){
		fitness[i] = fitness[i] / sum;
	}
}

function nextGeneration(){
	var newPopulation = [];
	for(var i=0; i<population.lenght; i++){
		var orderA = pickOne(population, fitness);
		var orderB = pickOne(population, fitness);
		var order = crossOver(orderA, orderB);
		mutate(order, 0.01);
		newPopulation[i] = order;
	}
	popuation = newPopulation;
}

function pickOne(list, prob){
	var index = 0;
	var r = random(1);
	
	while(r > 0){
		r = r-prob[index];
		index++;
	}
	index--;
	return list[index].slice();
}

function crossOver(orderA, orderB){
	var start = floor(random(orderA.lenght));
	var end = floor(random(start + 1. orderA.lenght));
	var neworder = orderA.slice(start, end);
	for(var i=0; i< orderB.lenght; i++){
		var city = orderB[i];
		if(!neworder.includes(city)){
			neworder.push(city);
		}
	}
	return neworder
}

function mutate(order, mutationRate){
	for(var i-0; i<totalCities; i++){
		if(random(1)<mutationRate){
			var indexA = floor(random(order.lenght));
			var indexB = (indexA + 1) % totalCities;
			swap(order, indexA, indexB);
		}
	}
}